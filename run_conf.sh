#!/bin/bash
# this script will run configure with some standard
# edit this for the install dir:
prefix=/usr/local

# valid precision is either float or double:
precision=double

# base confopts:
# note that this file is duplicated in xgrafix and xoopic, so a couple of 
# values are overspecified - ignore warnings like 'urecognized options with-SCALAR'

confopts="--prefix=$prefix --with-SCALAR=$precision --enable-fulloptimize "

# MPI :
    # confopts="$confopts --enable-MPI --with-CC=mpicc --with-CXX=mpicxx"

if [ `uname -s` == 'Darwin' ]; then
  # for Mac OS (condition added by JK, 2015-11-09)
  # extra="/sw" 
  extra="/opt/local" # for macports
  confopts="$confopts --with-tclconfig=$extra/lib --with-tkconfig=$extra/lib --with-dfftw-incdir=$extra/include --with-sfftw-incdir=$extra/include --with-hdf5-dir=$extra"

elif [ `uname -n` == 'fermi3d-ptsg' ]; then
  # for Fermi @ PTSG (condition added by JK, 2017-11-02)
  libdir="/usr/lib/x86_64-linux-gnu/"
  incdir="/usr/include/"
  tcltkver="8.6"
  confopts="$confopts --with-xpm=${libdir} --with-tclconfig=${libdir}/tcl${tcltkver} --with-tclhdir=${incdir}/tcl${tcltkver} --with-tkconfig=${libdir}/tk${tcltkver} --with-tkhdir=${incdir}/tk${tcltkver}"

elif [ `lsb_release -d | awk '{ print $2 }'` == 'Ubuntu' ]; then
  # for Ubuntu 18.04 (condition added by JK, 2018-10-22)
  libdir="/usr/lib/x86_64-linux-gnu/"
  confopts="$confopts --with-xpm=${libdir}"

elif [ `lsb_release -d | awk '{ print $2 }'` == 'openSUSE' ]; then
  # for openSUSE Leap 15.0 (condition added by JK, 2018-11-27)
  # target lib dir is /usr/local/lib and not lib64 (which is by default)
  confopts="$confopts --libdir=/usr/local/lib"
fi

# I recommend getting dependencies for xoopic through macports (macports.org)
# I find thatxgrafix will build but will crash if you link against the system
# tcl/tk.  I'm not sure why, and this may be fixed at some point, but 
# currently my best solution has been using macports for tcl/tk and other deps.
# once you have macports, you can get the dependencies
# with the following:

# sudo port install imagemagick +no_x11 bison fftw automake libpng tcl tk
# add 'hdf5' to the end if you also want hdf5

# I also recommend getting gfortran / gcc 4.2 from AT&T:
# http://r.research.att.com/tools/
# 
# changed by JK, 2015-11-09

for i in /usr/lib64 /usr/local/lib64 /usr/lib/x86_64-linux-gnu/; do
	# check for some common library paths
	if [ -d $i ]; then
	    # make sure 64-bit lib is checked for libraries
	    export PATH=$PATH:$i
	fi
done

confopts="$confopts $@"

echo "configuring with options: $confopts"
./configure $confopts && \
echo "

Configure successful!
 if you have a multicore machine, now you can build with:
 'make -j 4' (for 4 threaded compiling)
 and install with:
 'make install' or 'sudo make install', depending on whether you have write
  permissions to $prefix"
